import os, sys, re

class SteamAPI():
    def __init__(self):
        self.tileImageDir    = "tiles"
        self.steamGamesPaths = []
        self.games           = None

    def findSteamGames(self):
        '''
        look for the path to the steam games.  
        '''
        with open("C:\Program Files (x86)\Steam\steamapps\libraryfolders.vdf", 'r') as libraryfolders:
            pass

    def getGames(self):
        '''
        for each discovered manifest file, create a SteamGame object
        '''
        for path in self.steamGamesPaths:
            self.games.append(SteamGame(path))

class SteamGame():
    def __init__(self, manifestPath):
        self.manifestPath  = manifestPath
        self.launchURL     = None
        self.tileImagePath = None
        self.steamID       = None
        self.name          = None

        self.getInfoFromManifest()

    def getInfoFromManifest(self):
        with open(self.manifestPath, 'r') as manifest:
            for line in manifest:
                r = re.search(r"\"appid\"\s+\"(\d+)\"", line)
                if r and r.group(1):
                    self.steamID = r.group(1)

                r = re.search(r"\"name\"\s+\"(.*)\"", line)
                if r and r.group(1):
                    self.name = r.group(1)

    def getTileImage(self):
        imageURL = "http://cdn.akamai.steamstatic.com/steam/apps/{steamID}/header.jpg".format(self.steamID)
