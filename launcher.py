# python modules

# 3rd party modules
import xbmcplugin
import xbmcaddon

# local modules
from steam_api import *


def main():
    steam = SteamApi()

    for game in steam.games:
        xbmcplugin.addDirectoryItem(int(sys.argv[1]), game.launchURL, xbmcaddon.ListItem(game.name), totalItems=50)


if __name__ == "__main__":
    main()